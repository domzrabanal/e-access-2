import * as api from './../../config';
import * as types from './../../mutation-types';

export default {
    state: {
        leave_credits: [],
        pending_request: [],
        pagination: [],
        leave_requests: [],
        show_request_leave_dialog: false,
        leave_request: []
    },
    mutations: {
        [types.SET_LEAVE_CREDITS] (state, payload) {
            state.leave_credits = payload.leave_credits
        },
        [types.SET_PENDING_REQUESTS] (state, payload) {
            state.pending_request = payload.pending_request
            state.pagination = payload.pagination
        },
        [types.SET_LEAVE_REQUESTS] (state, payload) {
            state.leave_requests = payload.leave_requests
            state.pagination = payload.pagination
        },
        [types.SHOW_REQUEST_LEAVE_DIALOG] (state, payload) {
            state.show_request_leave_dialog = true
        },
        [types.SET_LEAVE_REQUEST_DETAIL] (state, payload) {
            state.leave_request = payload.leave_request
        }
    },
    actions: {
        getLeaveCredits: ({commit, dispatch}, data) => {
            axios.get(api.leaveCredits, {
                params: data
            })
            .then(response => {
                commit({
                    type: types.SET_LEAVE_CREDITS,
                    leave_credits: response.data
                })
            })
            .catch(error => {

            })
        },
        getMyPendingRequests: ({commit, dispatch}, data) => {
            axios.get(api.myPendingRequests, {
                    params: data
                })
                .then(response => {
                    commit({
                        type: types.SET_PENDING_REQUESTS,
                        pending_request: response.data.data,
                        pagination: response.data
                    })
                })
                .catch(error => {

                })
        },
        getLeaveRequests: ({commit, dispatch}, data) => {
            axios.get(api.leaveRequests, {
                params: data
            })
            .then(response => {
                response.data.per_page = parseInt(response.data.per_page)
                commit({
                    type: types.SET_LEAVE_REQUESTS,
                    leave_requests: response.data.data,
                    pagination: response.data
                })
            })
            .catch(error => {

            })
        },
        postChangeRequestStatus: ({dispatch}, formData) => {
            return new Promise((resolve, reject) => {
                axios.post(api.changeLeaveStatus, formData)
                    .then(response => {
                        dispatch('showSuccessNotification', response.data)
                        resolve();
                    })
                    .catch(error => {
                        dispatch('showErrorNotification', error.data);
                        reject()
                    })
            })
        },
        showRequestLeaveDialog({commit}) {
            commit({
                type: types.SHOW_REQUEST_LEAVE_DIALOG
            })
        },
        postRequestLeave: ({dispatch}, formData) => {
            return new Promise((resolve, reject) => {
                axios.post(api.requestLeave, formData)
                .then(response => {
                    dispatch('showSuccessNotification', response.data)
                    resolve();
                })
                .catch(error => {
                    dispatch('showErrorNotification', error.response.data);
                    reject()
                })
            })
        },
        getLeaveDetail: ({commit, dispatch}, data) => {
            axios.get(api.getLeaveRequest + '/' + data.id)
            .then(response => {
                commit({
                    type: types.SET_LEAVE_REQUEST_DETAIL,
                    leave_request: response.data
                })
            })
            .catch(error => {

            })
        },
        postUpdateLeave: ({dispatch}, formData) => {
            return new Promise((resolve, reject) => {
                axios.post(api.updateLeave + '/' + formData.id, formData)
                    .then(response => {
                        dispatch('showSuccessNotification', response.data)
                        resolve();
                    })
                    .catch(error => {
                        dispatch('showErrorNotification', error.response.data);
                        reject()
                    })
            })
        }
    }
}