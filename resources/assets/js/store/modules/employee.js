import * as api from './../../config';
import * as types from './../../mutation-types';

export default {
    state: {
        members: [],
        pagination: [],
        pending_requests: [],
        employee: []
    },
    mutations: {
        [types.SET_MEMBERS] (state, payload) {
            state.members =  payload.members
            state.pagination = payload.pagination
        },
        [types.SET_MEMBERS_PENDING_REQUESTS] (state, payload) {
            state.pending_requests = payload.pending_requests,
            state.pagination = payload.pagination
        },
        [types.SET_EMPLOYEE] (state, payload) {
            state.employee = payload.employee
        }
    },
    actions: {
        getMembers: ({commit, dispatch}, data) => {
            axios.get(api.members, {
                params: data
            }).then(response => {
                response.data.per_page = parseInt(response.data.per_page)
                commit({
                    type: types.SET_MEMBERS,
                    members: response.data.data,
                    pagination: response.data
                })
            })
            .catch(error => {

            })
        },
        getLeaveCreditsByUser: ({commit, dispatch}, data) => {
            axios.get(api.leaveCredits, {
                params: data
            })
            .then(response => {
                commit({
                    type: types.SET_LEAVE_CREDITS,
                    leave_credits: response.data
                })
            })
            .catch(error => {

            })
        },
        getMembersPendingList: ({commit, dispatch}, data) => {
            axios.get(api.membersPendingRequest, {
                params: data
            }).then(response => {
                response.data.per_page = parseInt(response.data.per_page)
                commit({
                    type: types.SET_MEMBERS_PENDING_REQUESTS,
                    pending_requests: response.data.data,
                    pagination: response.data
                })
            })
            .catch(error => {

            })
        },
        getEmployeeDetail: ({commit, dispatch}, data) => {
            axios.get(api.employeeDetail + '/' + data.id)
            .then(response => {
                commit({
                    type: types.SET_EMPLOYEE,
                    employee: response.data
                })
            })
            .catch(error => {

            })
        }
    }
}