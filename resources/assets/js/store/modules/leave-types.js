import * as api from './../../config';
import * as types from './../../mutation-types';

export default {
    state: {
        leave_types: []
    },
    mutations: {
        [types.SET_LEAVE_TYPES] (state, payload) {
            state.leave_types = payload.leave_types
        }
    },
    actions: {
        getLeaveTypes: ({commit, dispatch}) => {
            axios.get(api.leaveTypes)
                .then(response => {
                    commit({
                        type: types.SET_LEAVE_TYPES,
                        leave_types: response.data
                    })
                })
                .catch(error => {

                })
        }
    }
}