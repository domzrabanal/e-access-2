import Vue from 'vue';
import VueRouter from 'vue-router';
import jwtToken from './helpers/jwt-token';

Vue.use(VueRouter);

import Store from './store/index'
import Home from './components/home/Home.vue'
import Login from './components/login/Login.vue'
import ProfileWrapper from './components/profile-wrapper/ProfileWrapper.vue'
import Profile from './components/profile/Profile.vue'
import EditProfile from './components/edit-profile/EditProfile.vue'
import EditPassword from './components/edit-password/EditPassword.vue'
import LeaveCreditsWrapper from './components/leave-credits/LeaveCreditsWrapper.vue'
import MyLeaveCredits from './components/leave-credits/MyLeaveCredits.vue'
import RequestLeave from './components/leave-credits/RequestLeave.vue'
import EditLeave from './components/leave-credits/EditLeave.vue'
import MyApprovedLeaves from './components/leave-credits/MyApprovedLeaves.vue'
import MyPendingLeaves from './components/leave-credits/MyPendingLeaves.vue'
import MyDisapprovedLeaves from './components/leave-credits/MyDisapprovedLeaves.vue'
import MyCancelledLeaves from './components/leave-credits/MyCancelledLeaves.vue'
import TeamWrapper from './components/team/TeamWrapper.vue'
import EmployeesList from './components/team/EmployeesList.vue'
import TeamPendingLeaves from './components/team/PendingLeaves.vue'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Login,
            meta: { requiresGuest: true }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { requiresGuest: true }
        },
        {
            path: '/profile',
            component: ProfileWrapper,
            children: [
                {
                    path: '',
                    name: 'profile',
                    component: Profile,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'edit-profile',
                    name: 'profile.editProfile',
                    component: EditProfile,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'edit-password',
                    name: 'profile.editPassword',
                    component: EditPassword,
                    meta: { requiresAuth: true }
                }
            ]
        },
        {
            path: '/leave',
            component: LeaveCreditsWrapper,
            children: [
                {
                    path: 'credits',
                    name: 'leave.myLeaveCredits',
                    component: MyLeaveCredits,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'request',
                    name: 'leave.requestLeave',
                    component: RequestLeave,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'edit/:id',
                    name: 'leave.editLeave',
                    component: EditLeave,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'approved',
                    name: 'leave.approvedLeaves',
                    component: MyApprovedLeaves,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'pending',
                    name: 'leave.pendingLeaves',
                    component: MyPendingLeaves,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'disapproved',
                    name: 'leave.disapprovedLeaves',
                    component: MyDisapprovedLeaves,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'cancelled',
                    name: 'leave.cancelledLeaves',
                    component: MyCancelledLeaves,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'request-in-behalf/:id',
                    name: 'leave.requestLeaveInBehalf',
                    component: RequestLeave,
                    meta: { requiresAuth: true }
                },
            ]
        },
        {
            path: '/team',
            component: TeamWrapper,
            children: [
                {
                    path: 'members',
                    name: 'team.membersList',
                    component: EmployeesList,
                    meta: { requiresAuth: true }
                },
                {
                    path: 'pending-leaves',
                    name: 'team.pendingLeaves',
                    component: TeamPendingLeaves,
                    meta: { requiresAuth: true }
                }
            ]
        }
    ]
});

router.beforeEach((to, from, next) => {
    Store.dispatch('hideErrorNotification');
    Store.dispatch('hideSuccessNotification');

    if(to.meta.requiresAuth) {
        if(Store.state.authUser.authenticated || jwtToken.getToken())
            return next();
        else
            return next({name: 'index'});
    }
    if(to.meta.requiresGuest) {
        if(Store.state.authUser.authenticated || jwtToken.getToken())
            return next({name: 'index'});
        else
            return next();
    }
    next();
});

export default router;
