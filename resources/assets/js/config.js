const apiDomain = Laravel.apiDomain;
export const siteName = 'E-Access';

export const login = apiDomain + '/authenticate';
export const currentUser = apiDomain + '/user';
export const updateUserProfile = apiDomain + '/user/profile/update';
export const updateUserPassword = apiDomain + '/user/password/update';
export const profile = apiDomain + '/user/profile';

export const leaveCredits = apiDomain + '/leave/leave-credits';
export const myPendingRequests = apiDomain + '/leave/pending-requests';
export const leaveRequests = apiDomain + '/leave/get-requests';
export const changeLeaveStatus = apiDomain + '/leave/change-status';
export const requestLeave = apiDomain + '/leave/request-leave';
export const updateLeave = apiDomain + '/leave/edit';
export const getLeaveRequest = apiDomain + '/leave';
export const membersPendingRequest = apiDomain + '/leave/members-requests';

export const leaveTypes = apiDomain + '/leave-types';

export const members = apiDomain + '/employee/members';
export const employeeDetail = apiDomain + '/employee';