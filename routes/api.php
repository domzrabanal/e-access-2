<?php

Route::post('authenticate', 'AuthenticateController@authenticate');

Route::group(['middleware' => 'jwt.auth'], function()
{
    Route::get('user', 'UserController@show');
    Route::get('user/profile', 'UserController@getProfile');
    Route::post('user/profile/update', 'UserController@updateProfile');
    Route::post('user/password/update', 'UserController@updatePassword');

    Route::get('leave/leave-credits', 'LeaveController@getLeaveCredits');
    Route::get('leave/pending-requests', 'LeaveController@getPendingRequests');
    Route::get('leave/get-requests', 'LeaveController@getLeaveRequests');
    Route::post('leave/change-status', 'LeaveController@postChangeRequestStatus');
    Route::post('leave/request-leave', 'LeaveController@postRequestLeave');
    Route::get('leave/members-requests', 'LeaveController@getMembersPendingRequests');
    Route::get('leave/{id}', 'LeaveController@getLeave');
    Route::post('leave/edit/{id}', 'LeaveController@postUpdateLeave');

    Route::get('leave-types', 'LeaveTypeController@getAll');

    Route::get('employee/members', 'EmployeeController@getMembers');
    Route::get('employee/{id}', 'EmployeeController@getEmployee');
});
