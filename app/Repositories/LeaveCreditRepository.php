<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/23/2018
 * Time: 7:06 PM
 */

namespace App\Repositories;


use App\Models\LeaveCredit;

class LeaveCreditRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new LeaveCredit();
    }
}