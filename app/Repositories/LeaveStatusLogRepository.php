<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/24/2018
 * Time: 10:18 PM
 */

namespace App\Repositories;


use App\Models\LeaveStatusLog;

class LeaveStatusLogRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new LeaveStatusLog();
    }
}