<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/18/2018
 * Time: 8:27 PM
 */

namespace App\Repositories;

use App\User;

class UserRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new User();
    }
}