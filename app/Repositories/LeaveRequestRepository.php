<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/23/2018
 * Time: 7:05 PM
 */

namespace App\Repositories;

use App\Models\LeaveRequest;
use App\Models\LeaveStatus;
use App\User;
use Illuminate\Support\Facades\DB;

class LeaveRequestRepository extends AbstractRepository
{
    protected $model;

    public function __construct()
    {
        $this->model = new LeaveRequest();
    }

    public function getRequests($user_id, $leave_type_id, $leave_status_id, $date_range, $page, $limit)
    {
        $query = $this->model->select('leave_requests.id', 'leave_requests.remarks')
            ->addSelect(DB::raw('DATE_FORMAT(leave_requests.created_at, "%b %e, %Y") AS date_added'))
            ->addSelect(DB::raw('DATE_FORMAT(leave_requests.date_time_from, "%b %e, %Y %h:%i %p") AS date_from'))
            ->addSelect(DB::raw('DATE_FORMAT(leave_requests.date_time_to, "%b %e, %Y %h:%i %p") AS date_to'))
            ->addSelect(DB::raw('CONCAT(duration," hour(s)") AS duration'))
            ->addSelect('leave_types.name AS leave_type_name')
            ->addSelect(DB::raw("IF(DATEDIFF(date_time_from, CURDATE()) >= 0, TRUE, FALSE) 'editable'"))
            ->join('leave_types', 'leave_types.id', '=', 'leave_requests.leave_type_id')
            ->where('user_id', $user_id)
            ->whereRaw(DB::raw("YEAR(date_time_from) = YEAR(CURDATE())"));

        if (in_array(LeaveStatus::PENDING, $leave_status_id) && in_array(LeaveStatus::TL_APPROVED, $leave_status_id)) {
            $query->addSelect(DB::raw('IF(leave_status_id = 1,"Pending TL approval", IF(leave_status_id = 2,"Pending Admin approval","")) AS status'));
        }

        if (!is_null($leave_type_id)) {
            $query->where('leave_type_id', $leave_type_id);
        }

        if (!is_null($leave_status_id)) {
            $query->whereIn('leave_status_id', [$leave_status_id]);
        }

        if (!is_null($date_range)) {
            $query->whereBetween('date_time_from', $date_range);
        }
        $order = (in_array(LeaveStatus::ADMIN_APPROVED,$leave_status_id)) ? 'DESC' : 'ASC';
        return $query->orderBy('date_time_from', $order)->paginate($limit, $page);
    }

    public function checkForConflict($id, $data)
    {
        $query = $this->model->select('*')
            ->where('user_id', $data['user_id'])
            ->where(function($qry) use ($data) {
                $qry->whereBetween('date_time_from', $data['date_range'])
                    ->orWhereBetween('date_time_to', $data['date_range']);
            })
            ->whereNotIn('leave_status_id', [4, 5]);

        if (!is_null($id)) {
            $query->where('id', '!=', $id);
        }

        return $query->first();
    }

    public function getApprovedLeavesByLeaveType($leave_type_id, $user_id)
    {
        $query = $this->model->select(DB::raw("SUM(duration) as 'leave_count'"))
            ->where('leave_status_id', '=', 3)
            ->where('user_id', $user_id)
            ->where('leave_type_id', $leave_type_id)
            ->whereRaw(DB::raw("YEAR(date_time_from) = YEAR(CURDATE())"))
            ->first();

        return $query ? $query->leave_count : 0;
    }

    public function getMembersPendingRequest($user_id, $role, $leave_type_id, $leave_status_id, $date_range, $page, $limit)
    {
        $query = $this->model->select('leave_requests.id', 'leave_requests.remarks')
            ->addSelect(DB::raw('DATE_FORMAT(leave_requests.created_at, "%b %e, %Y") AS date_added'))
            ->addSelect(DB::raw('DATE_FORMAT(leave_requests.date_time_from, "%b %e, %Y %h:%i %p") AS date_from'))
            ->addSelect(DB::raw('DATE_FORMAT(leave_requests.date_time_to, "%b %e, %Y %h:%i %p") AS date_to'))
            ->addSelect(DB::raw('CONCAT(duration," hour(s)") AS duration'))
            ->addSelect('leave_types.name AS leave_type_name')
            ->addSelect(DB::raw('CONCAT(empfname,", ",empgname) AS full_name'))
            ->addSelect(DB::raw("IF(DATEDIFF(date_time_from, CURDATE()) >= 0, TRUE, FALSE) 'editable'"))
            ->join('leave_types', 'leave_types.id', '=', 'leave_requests.leave_type_id')
            ->join('users', 'users.id', '=', 'leave_requests.user_id')
            ->join('employees', 'employees.empid', '=', 'users.userid')
            ->whereRaw(DB::raw("YEAR(date_time_from) = YEAR(CURDATE())"))
            ->addSelect(DB::raw('IF(leave_status_id = 1,"Pending TL approval", IF(leave_status_id = 2,"Pending Admin approval","")) AS status'));

        if ($role == User::TL_ROLE) {
            $query->where('employees.posid_man', $user_id);
        } else {
            $query->where('employees.empid', '!=', $user_id);
        }

        if (!is_null($leave_type_id)) {
            $query->where('leave_type_id', $leave_type_id);
        }

        if (!is_null($leave_status_id)) {
            $query->whereIn('leave_status_id', [$leave_status_id]);
        }

        if (!is_null($date_range)) {
            $query->whereBetween('date_time_from', $date_range);
        }

        return $query->orderBy('date_time_from', 'ASC')->paginate($limit, $page);
    }
}