<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/23/2018
 * Time: 2:25 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LeaveCreditsDetails extends Model
{
    protected $table = 'leavecredits_details';

}