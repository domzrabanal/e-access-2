<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/23/2018
 * Time: 2:53 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LeaveStatusLog extends Model
{
    protected $table = 'leave_status_logs';

    protected $timestamp = false;

    protected $fillable = [
        'leave_request_id',
        'leave_status_id',
        'reason'
    ];
}