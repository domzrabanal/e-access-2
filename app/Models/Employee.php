<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/18/2018
 * Time: 4:57 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    protected $primaryKey = 'empid';

    public $timestamps = false;

    const EMPLOYEE_STATUS_REGULAR = 'REGULAR';
    const EMPLOYEE_STATUS_RESIGNED = 'RESIGNED';
    const EMPLOYEE_STATUS_PROBATIONARY = 'PROBATIONARY';

    public function user()
    {
        return $this->hasOne('user');
    }

    public function getFullNameAttribute()
    {
        return $this->empfname . ', ' . $this->empgname;
    }

    public function leaveCredits()
    {
        return $this->hasOne('App\Models\LeaveCredit', 'empid');
    }
}