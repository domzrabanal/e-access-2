<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/22/2018
 * Time: 10:09 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class LeaveCredit extends Model
{
    protected $table = 'leavecredits';

    public $timestamps = false;

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee');
    }
}