<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/23/2018
 * Time: 2:07 PM
 */

namespace App\Services;


use App\Repositories\LeaveCreditRepository;
use App\Repositories\LeaveRequestRepository;
use App\Repositories\LeaveStatusLogRepository;
use App\Repositories\LeaveTypeRepository;
use App\Repositories\UserRepository;

class LeaveService
{
    protected $leaveRequestRepository;
    protected $leaveCreditRepository;
    protected $leaveTypeRepository;
    protected $leaveStatusLogRepository;
    protected $userRepository;

    public function __construct()
    {
        $this->leaveRequestRepository = new LeaveRequestRepository();
        $this->leaveCreditRepository = new LeaveCreditRepository();
        $this->leaveTypeRepository = new LeaveTypeRepository();
        $this->leaveStatusLogRepository = new LeaveStatusLogRepository();
        $this->userRepository = new UserRepository();
    }

    public function getLeaveCredits($user)
    {
        $leave_credits = $user->employee->leaveCredits;
        $leave_consumption = $this->leaveTypeRepository->getLeaveUsageForCurrentYear($user->id);

        foreach ($leave_consumption as $detail) {
            if ($detail->id == 1) {
                $used = (!is_null($detail->used) && $detail->used > 0) ? $detail->used : 0;
                $available = ($leave_credits->totalsldays * 8) + $leave_credits->totalslhours;
                $remaining = $available - $used;

                $detail->available = $leave_credits->totalsldays . ' day(s) ' . $leave_credits->totalslhours . ' hour(s)';
                $detail->remaining = floor($remaining / 8) . ' day(s) ' . ($remaining % 8) . ' hour(s)';
                $detail->used = floor($used / 8) . ' day(s) ' . ($used % 8) . ' hour(s)';
            } else if ($detail->id == 2) {
                $used = (!is_null($detail->used) && $detail->used > 0) ? $detail->used : 0;
                $available = ($leave_credits->totalvldays * 8) + $leave_credits->totalvlhours;
                $remaining = $available - $used;

                $detail->available = $leave_credits->totalvldays . ' day(s) ' . $leave_credits->totalvlhours . ' hour(s)';
                $detail->remaining = floor($remaining / 8) . ' day(s) ' . ($remaining % 8) . ' hour(s)';
                $detail->used = floor($used / 8) . ' day(s) ' . ($used % 8) . ' hour(s)';
            } else {
                $used = (!is_null($detail->used) && $detail->used > 0) ? $detail->used : 0;

                $detail->available = 0;
                $detail->remaining = 0;
                $detail->used = floor($used / 8) . ' day(s) ' . ($used % 8) . ' hour(s)';
            }
        }


        return $leave_consumption;
    }

    public function getRequests($user_id, $leave_type_id, $leave_status_id, $date_range, $page, $limit)
    {
        return $this->leaveRequestRepository->getRequests($user_id, $leave_type_id, $leave_status_id, $date_range, $page, $limit);
    }

    public function createRequest($data)
    {
        return $this->leaveRequestRepository->create($data);
    }

    public function updateRequest($id, $data)
    {
        return $this->leaveRequestRepository->update($id, $data);
    }

    public function addStatusLog($data)
    {
        return $this->leaveStatusLogRepository->create($data);
    }

    public function checkForConflict($id, $data)
    {
        return $this->leaveRequestRepository->checkForConflict($id, $data);
    }

    public function getRemainingLeaveCreditsByType($leave_type_id, $user_id)
    {
        $employee_id = $this->userRepository->find($user_id)->userid;
        $leave_credits = $this->leaveCreditRepository->findBy(['empid' => $employee_id])->first();
        $total_leave_hours = $this->leaveRequestRepository->getApprovedLeavesByLeaveType($leave_type_id, $user_id);
        $remaining_credits = ($leave_type_id == 1) ? ($leave_credits->totalsldays * 8) - $total_leave_hours : (($leave_credits->totalvldays * 8) + 40) - $total_leave_hours;

        return $remaining_credits;
    }

    public function getLeaveRequestById($id)
    {
        return $this->leaveRequestRepository->find($id);
    }

    public function getUserById($user_id)
    {
        return $this->userRepository->find($user_id);
    }

    public function getMembersPendingRequest($user_id, $role, $page, $limit, $leave_type_id, $leave_status_id, $date_range)
    {
        return $this->leaveRequestRepository->getMembersPendingRequest($user_id, $role, $leave_type_id, $leave_status_id, $date_range, $page, $limit);
    }
}