<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/24/2018
 * Time: 3:10 PM
 */

namespace App\Http\Controllers;


use App\Repositories\LeaveTypeRepository;

class LeaveTypeController extends Controller
{
    public $leaveTypeRepository;

    public function __construct()
    {
        $this->leaveTypeRepository = new LeaveTypeRepository();
    }

    public function getAll()
    {
        try {
            return $this->leaveTypeRepository->all();
        } catch (\Exceptione $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }
}