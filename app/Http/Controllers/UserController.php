<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function show(Request $request)
    {
        return $request->user();
    }

    public function getProfile(Request $request)
    {
        try {
            $user = Auth::user();
            $user->employee;
            $user->full_name = $user->employee->empfname . ', ' . $user->employee->empgname;
            return $user;
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }
    
    public function updateProfile(Request $request)
    {
        $rules = [
            'name'  =>  'required',
            'email' =>  'required|email|',
        ];

        $this->validate($request, $rules);
        
        $user = $request->user();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        return response()->json(compact('user'));
    }

    public function updatePassword(Request $request)
    {
        $rules = [
            'new_password'          =>  'required',
            'confirm_new_password'  =>  'required|same:new_password'
        ];

        $this->validate($request, $rules);

        $user = $request->user();
        $user->password = bcrypt($request->input('new_password'));
        $user->saveOrFail();

        return response()->json(compact('user'));
    }
}