<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 6/5/2018
 * Time: 9:15 PM
 */

namespace App\Http\Controllers;


use App\Repositories\EmployeeRepository;
use App\Services\EmployeeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    protected $employeeRepository;
    protected $employeeService;
    
    public function __construct()
    {
        $this->employeeRepository = new EmployeeRepository();
        $this->employeeService = new EmployeeService();
    }

    public function getMembers(Request $request)
    {
        try {
            $page = ($request->input('page')) ? $request->input('page') : 1;
            $limit = ($request->input('limit')) ? $request->input('limit'): 10;
            $keyword = ($request->input('keyword')) ? trim($request->input('keyword')) : null;

            $members = $this->employeeRepository->getMembersBySupervisor(Auth::user(), $page, $limit, $keyword);

            return $members;
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }

    public function getEmployee($id)
    {
        try {
            $user = $this->employeeService->getUser($id);

            if (!$user) {
                return response()->json('User not found!')->setStatusCode(404);
            }

            $employee = $user->employee;
            $employee->name = ucwords(strtolower($employee->getFullNameAttribute()));

            return response()->json($employee)->setStatusCode(200);

        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }
}