<?php
/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/22/2018
 * Time: 10:07 PM
 */

namespace App\Http\Controllers;


use App\Models\LeaveStatus;
use App\Models\LeaveType;
use App\Services\LeaveService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LeaveController extends Controller
{
    protected $leaveService;

    public function __construct()
    {
        $this->leaveService = new LeaveService();
    }

    public function getLeaveCredits(Request $request)
    {
        try {
            $user = ($request->input('user_id')) ? $this->leaveService->getUserById($request->input('user_id')) : Auth::user();

            $leave_credits = $this->leaveService->getLeaveCredits($user);

            return $leave_credits;
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }

    public function getLeaveRequests(Request $request)
    {
        try {
            $user_id = ($request->input('user_id')) ? $request->input('user_id') : Auth::user()->id;
            $page = ($request->input('page')) ? $request->input('page') : 1;
            $limit = ($request->input('limit')) ? $request->input('limit'): 10;
            $leave_type = ($request->input('leave_type')) ? $request->input('leave_type') : null;
            $leave_status = ($request->input('leave_status')) ? explode(',', $request->input('leave_status')) : null;
            $date_range = ($request->input('date_filter')) ? $request->input('date_filter') : null;

            $leave_credits = $this->leaveService->getRequests($user_id, $leave_type, $leave_status, $date_range, $page, $limit);

            return $leave_credits;
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }

    public function postChangeRequestStatus(Request $request)
    {
        try {
            foreach ($request->input('id') as $leave_id) {
                $leave_request = $this->leaveService->getLeaveRequestById($leave_id);

                if (!$leave_request) {
                    return response()->json('Leave request not found!')->setStatusCode(400);
                }

                $user = Auth::user();
                $send_notification = ($user->id !== $leave_request->user_id) ? true : false;
                $status_id = ($user->userlevel == User::ADMIN_ROLE) ? LeaveStatus::TL_APPROVED : LeaveStatus::PENDING;

                if ($request->input('action') == 'approve') {
                    $status_id = ($user->userlevel == User::ADMIN_ROLE) ? LeaveStatus::ADMIN_APPROVED : LeaveStatus::TL_APPROVED;
                }

                if ($request->input('action') == 'disapprove') {
                    $status_id = LeaveStatus::DISAPPROVED;
                }

                if ($request->input('action') == 'cancel') {
                    $status_id = LeaveStatus::CANCELLED;
                }

                $this->leaveService->updateRequest($leave_id, ['leave_status_id' => $status_id]);

                $this->leaveService->addStatusLog([
                    'leave_request_id' => $leave_id,
                    'leave_status_id' => $status_id,
                    'reason' => $request->input('remarks')
                ]);
            }

            $message = '';

            if ($status_id == LeaveStatus::ADMIN_APPROVED || $status_id == LeaveStatus::TL_APPROVED) {
                $message = 'Leave Request successfully approved!';
            }

            if ($status_id == LeaveStatus::DISAPPROVED) {
                $message = 'Leave Request successfully disapproved!';
            }

            if ($status_id == LeaveStatus::CANCELLED) {
                $message = 'Leave Request successfully cancelled!';
            }

            return response()->json($message)->setStatusCode(200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }

    public function postRequestLeave(Request $request)
    {
        try {
            $data = $request->all();
            $auth_user = Auth::user();

            if ($data['user_id'] != $auth_user->id) {
                $leave_user = $this->leaveService->getUserById($data['user_id']);
                $data['leave_status_id'] = ($leave_user->userlevel == User::ADMIN_ROLE) ? LeaveStatus::TL_APPROVED : LeaveStatus::PENDING;
            } else {
                $data['leave_status_id'] = ($auth_user->userlevel == User::ADMIN_ROLE) ? LeaveStatus::TL_APPROVED : LeaveStatus::PENDING;
            }

            $data['duration'] = ($data['is_full_shift']) ? 8 : $data['duration'];

            $validate = $this->validateLeave(null, $data);

            if (!$validate->is_valid) {
                return response()->json($validate->message)->setStatusCode(400);
            }

            $inputted_start_date = Carbon::parse($data['date_range'][0]);
            $inputted_end_date = Carbon::parse($data['date_range'][1]);
            $diff = $inputted_end_date->diff($inputted_start_date);

            unset($data['date_range']);

            $num_of_days = $diff->days + 1;

            for ($i = 0; $i < $num_of_days; $i++) {
                $end_date_format = "Y-m-d {$inputted_end_date->format('H:i:s')}";

                if ((int) $inputted_start_date->format('H') > (int) $inputted_end_date->format('H')) {
                    $start_date = $inputted_start_date;
                    $data['date_time_from'] = $start_date->format('Y-m-d H:i:s');
                    $end_date = $start_date->addDay();
                    $data['date_time_to'] = $end_date->format($end_date_format);
                    $end_date_day_name = $end_date->format('l');
                } else {
                    $start_date = ($i == 0) ? $inputted_start_date : $inputted_start_date->addDay();
                    $end_date = $start_date;
                    $data['date_time_from'] = $start_date->format('Y-m-d H:i:s');
                    $data['date_time_to'] = $inputted_start_date->format($end_date_format);
                    $end_date_day_name = $end_date->format('l');
                }

                if (($end_date_day_name == 'Saturday' && (int) $inputted_end_date->format('H') < 7) ||
                !in_array($end_date_day_name, ['Saturday', 'Sunday'])) {
                    $leave = $this->leaveService->createRequest($data);

                    $this->leaveService->addStatusLog([
                        'leave_request_id' => $leave->id,
                        'leave_status_id' => $data['leave_status_id'],
                        'reason' => ($auth_user->id != $leave->user_id) ? 'Leave added by '.$auth_user->employee->getFullNameAttribute() : null
                    ]);
                }
            }

            return response()->json('Leave Request successfully added!')->setStatusCode(200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }

    public function getLeave($id)
    {
        try {
            $leave_request = $this->leaveService->getLeaveRequestById($id);

            if (!$leave_request) {
                return response()->json('Leave request not found!')->setStatusCode(400);
            }

            return response()->json($leave_request)->setStatusCode(200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }

    public function postUpdateLeave($id, Request $request)
    {
        try {
            $data = $request->all();
            $data['duration'] = ($data['is_full_shift']) ? 8 : $data['duration'];
            $leave_request = $this->leaveService->getLeaveRequestById($id);

            if (!$leave_request) {
                return response()->json('Leave request not found!')->setStatusCode(400);
            }

            $validate = $this->validateLeave($id, $data);

            if (!$validate->is_valid) {
                return response()->json($validate->message)->setStatusCode(400);
            }

            $inputted_start_date = Carbon::parse($data['date_range'][0]);
            $inputted_end_date = Carbon::parse($data['date_range'][1]);
            $diff = $inputted_end_date->diff($inputted_start_date);

            unset($data['date_range']);

            $num_of_days = $diff->days + 1;

            for ($i = 0; $i < $num_of_days; $i++) {
                $end_date_format = "Y-m-d {$inputted_end_date->format('H:i:s')}";

                if ((int) $inputted_start_date->format('H') > (int) $inputted_end_date->format('H')) {
                    $start_date = $inputted_start_date;
                    $data['date_time_from'] = $start_date->format('Y-m-d H:i:s');
                    $end_date = $start_date->addDay();
                    $data['date_time_to'] = $end_date->format($end_date_format);
                    $end_date_day_name = $end_date->format('l');
                } else {
                    $start_date = ($i == 0) ? $inputted_start_date : $inputted_start_date->addDay();
                    $end_date = $start_date;
                    $data['date_time_from'] = $start_date->format('Y-m-d H:i:s');
                    $data['date_time_to'] = $inputted_start_date->format($end_date_format);
                    $end_date_day_name = $end_date->format('l');
                }

                if (($end_date_day_name == 'Saturday' && (int) $inputted_end_date->format('H') < 7) ||
                    !in_array($end_date_day_name, ['Saturday', 'Sunday'])) {

                    if ($i == 0) {
                        $this->leaveService->updateRequest($id, $data);
                        $leave_id = $id;
                        $reason = 'Leave updated!';
                    } else {
                        $leave = $this->leaveService->createRequest($data);
                        $leave_id = $leave->id;
                        $reason = null;
                    }

                    $this->leaveService->addStatusLog([
                        'leave_request_id' => $leave_id,
                        'leave_status_id' => LeaveStatus::PENDING,
                        'reason' => $reason
                    ]);
                }
            }


            return response()->json('Leave Request successfully updated!')->setStatusCode(200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }

    public function validateLeave($id, $data)
    {
        if (empty($data['duration']) || $data['duration'] == 0 || !is_numeric($data['duration'])) {
            return (object) [
                'is_valid' => false,
                'message' => "Invalid duration value!"
            ];
        }

        if ($data['leave_type_id'] == LeaveType::SICK_LEAVE && is_float($data['duration'])) {
            return (object) [
                'is_valid' => false,
                'message' => "Sick Leave hours should only be whole hours. Extra minutes can be added via LWOP request."
            ];
        }

        if ($data['leave_type_id'] == LeaveType::VACATION_LEAVE && (!in_array($data['duration'], [4,8]))) {
            return (object) [
                'is_valid' => false,
                'message' => "Vacation Leave hours should only be 4hrs or 8hrs."
            ];
        }

        $conflict = $this->leaveService->checkForConflict($id, $data);

        if ($conflict) {
            $leave_type = $conflict->leaveType->name;
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $conflict->date_time_from)->toFormattedDateString();
            return (object) [
                'is_valid' => false,
                'message' => "You already filed a {$leave_type} on {$date}"
            ];
        }

        if (in_array($data['leave_type_id'], [LeaveType::SICK_LEAVE, LeaveType::VACATION_LEAVE])) {
            $remaining_leaves = $this->leaveService->getRemainingLeaveCreditsByType($data['leave_type_id'], $data['user_id']);

            if ($remaining_leaves == 0) {
                return (object) [
                    'is_valid' => false,
                    'message' => "You don't have enough leave credits left!"
                ];
            }
        }

        if (!is_null($id)) {
            $inputted_start_date = Carbon::parse($data['date_range'][0]);
            $inputted_end_date = Carbon::parse($data['date_range'][1]);
            $diff = $inputted_end_date->diff($inputted_start_date)->days;

            if ($diff > 0) {
                return (object) [
                    'is_valid' => false,
                    'message' => "You can't update your leave into multiple days."
                ];
            }
        }

        return (object) ['is_valid' => true];
    }

    public function getMembersPendingRequests(Request $request)
    {
        try {
            $user = Auth::user();
            $page = ($request->input('page')) ? $request->input('page') : 1;
            $limit = ($request->input('limit')) ? $request->input('limit'): 10;
            $leave_type = ($request->input('leave_type')) ? $request->input('leave_type') : null;
            $date_range = ($request->input('date_filter')) ? $request->input('date_filter') : null;
            $leave_status = ($user->userlevel == User::ADMIN_ROLE) ? LeaveStatus::TL_APPROVED : LeaveStatus::PENDING;

            $requests = $this->leaveService->getMembersPendingRequest($user->userid, $user->userlevel, $page, $limit, $leave_type, $leave_status, $date_range);

            return response()->json($requests)->setStatusCode(200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage())->setStatusCode(400);
        }
    }
}