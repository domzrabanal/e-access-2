<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveStatusLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_status_logs', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('leave_request_id')->unsigned();
            $table->integer('leave_status_id')->unsigned();
            $table->text('reason')->nullable();
            $table->timestamps();

            $table->foreign('leave_request_id')->references('id')->on('leave_requests')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('leave_status_id')->references('id')->on('leave_statuses')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_status_logs');
    }
}
