<?php
use App\Models\LeaveType;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/23/2018
 * Time: 3:14 PM
 */
class LeaveTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaveType::create([
            'name' => 'Sick Leave'
        ]);
        LeaveType::create([
            'name' => 'Vacation Leave'
        ]);
        LeaveType::create([
            'name' => 'LWOP'
        ]);
        LeaveType::create([
            'name' => 'Maternity Leave'
        ]);
        LeaveType::create([
            'name' => 'Paternity Leave'
        ]);
        LeaveType::create([
            'name' => 'Bereavement Leave'
        ]);
        LeaveType::create([
            'name' => 'Undertime'
        ]);
    }
}