<?php
use App\Models\LeaveStatus;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: drabanal
 * Date: 5/23/2018
 * Time: 3:08 PM
 */
class LeaveStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaveStatus::create([
            'name' => 'Pending'
        ]);
        LeaveStatus::create([
            'name' => 'TL Approved'
        ]);
        LeaveStatus::create([
            'name' => 'Admin Approved'
        ]);
        LeaveStatus::create([
            'name' => 'Disapproved'
        ]);
        LeaveStatus::create([
            'name' => 'Cancelled'
        ]);
    }
}