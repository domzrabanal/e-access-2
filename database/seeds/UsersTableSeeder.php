<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
            if ($user->employee) {
                $user->email = strtolower($user->employee->empemail);
                $user->password = bcrypt($user->userid);
                $user->save();
            }
        }
    }
}
